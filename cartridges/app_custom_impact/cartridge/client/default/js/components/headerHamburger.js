'use strict';

const { CLASSES, CONFIG, GlobalVars } = require('../utils/globals');
const { disableScroll } = require('../utils/globalFunctions');

const
  SELECTORS = {
    body: 'body',
    openMenuButton: '.js-open-menu',
    closeMenuButton: '.js-close-menu, .js-menu-overlay',
    menuWrapper: '.js-menu-wrapper',
    menuContainer: '.js-menu-container',
    openNextLevel: '.js-open-next-list',
    openPrevLevel: '.js-open-prev-list',
    categoriesMenuItem: '.js-categories-list',
    openCategoriesMobile: '.js-open-categories-mobile'
  };

let
  enabledMobileMenu = false;

const
  $menuWrapper = $(SELECTORS.menuWrapper),
  $menuContainer = $(SELECTORS.menuContainer),
  $openNextLevel = $(SELECTORS.openNextLevel),
  $openPrevLevel = $(SELECTORS.openPrevLevel),
  $openMenuButton = $(SELECTORS.openMenuButton),
  $closeMenuButton = $(SELECTORS.closeMenuButton),
  $openCategoriesMobile = $(SELECTORS.openCategoriesMobile);

/**
 * Show next menu/submenu
 *
 * @param {Object} el - DOM element, usually the .js-open-prev-list element
 * @param {Object} event - Jquery event object
 */
function showPrev(el, event) {
  if ($(el).parent().parent('ul').length) {
    event.preventDefault();

    $(el).parent().parent('ul').css('left', '-100%');
  }
}

/**
 * Show prev menu/submenu
 *
 * @param {Object} el - DOM element, usually the .js-open-next-list element
 * @param {Object} event - Jquery event object
 */
function showNext(el, event) {
  if ($(el).next('ul').length) {
    event.preventDefault();

    $(el).next('ul').css('left', 0);
  }
}

/**
 * Hide mobile menu depends on window width
 */
function checkHeaderHamburger() {
  let windowWidth = $(window).innerWidth();

  if (GlobalVars.openedMobileMenu && windowWidth > CONFIG.breakpointMedium) {
    $menuWrapper.removeClass(CLASSES.showMenu);
    $menuContainer.find('ul').removeAttr('style');

    GlobalVars.openedMobileMenu = false;
    enabledMobileMenu = false;
    disableScroll(false);
  }
}

module.exports = function () {
  $(window).on('resize', function () {
    checkHeaderHamburger();
  });

  $openMenuButton.click(function () {
    $menuWrapper.addClass(CLASSES.showMenu);

    GlobalVars.openedMobileMenu = true;
    enabledMobileMenu = true;
    disableScroll(true);
  });

  $closeMenuButton.click(function () {
    $menuWrapper.removeClass(CLASSES.showMenu);
    $menuContainer.find('ul').removeAttr('style');
    $openCategoriesMobile.removeClass(CLASSES.active);

    GlobalVars.openedMobileMenu = false;
    enabledMobileMenu = false;
    disableScroll(false);
  });

  $openNextLevel.click(function (event) {
    if (enabledMobileMenu) {
      showNext(this, event);
    }
  });

  $openPrevLevel.click(function (event) {
    if (enabledMobileMenu) {
      showPrev(this, event);
    }
  });
};

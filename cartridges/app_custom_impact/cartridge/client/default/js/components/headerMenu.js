'use strict';

const debounce = require('lodash/debounce');
const { CLASSES, CONFIG } = require('../utils/globals');

const
  SELECTORS = {
    topLevelCategoryItem: '.js-top-level-category',
    headerContainer: '.js-header-container',
    isShow: '.is-show',
    mainHeader: '.js-main-header',
    overlay: '.js-modal-background',
    secondLevelList: '.second-level-list',
    brandsImages: '.js-brands-image-lazy',
    window: window
  };

const
  EVENTS = {
    showMenu: 'showmenu',
    hideMenu: 'hidemenu'
  };

let $topLevelCategoryItems;
let $headerContainer;
let $mainHeader;

let lastEvent = null;

const setLastEvent = (event) => lastEvent = event;

const removeIsShowClasses = () => $topLevelCategoryItems.filter(SELECTORS.isShow).removeClass(CLASSES.isShow);

const hideMenu = function () {
  removeIsShowClasses();
  $mainHeader.removeClass(CLASSES.active);
};

const onCategoryMouseEnter = function() {
  const $this = $(this);

  if (!$this.hasClass(CLASSES.isShow) && lastEvent === EVENTS.showMenu) {
    removeIsShowClasses();

    $this.addClass(CLASSES.isShow);

    if ($this.find(SELECTORS.secondLevelList).length) {
      console.log(CLASSES.active);
      $mainHeader.addClass(CLASSES.active);
    }
  }
};

const onHeaderContainerMouseOver = function() {
  setLastEvent(EVENTS.hideMenu);
  hideMenu();
};

const onTopLevelCategoryItemsMouseLeave = function() {
  setLastEvent(EVENTS.hideMenu);
  hideMenu();
};

const initEvents = () => {
  const { headerMenuDebounceDelay } = CONFIG;

  $topLevelCategoryItems = $(SELECTORS.topLevelCategoryItem);
  $headerContainer = $(SELECTORS.headerContainer);
  $mainHeader = $(SELECTORS.mainHeader);


  $topLevelCategoryItems
    .mouseenter(() => {
      setLastEvent(EVENTS.showMenu);
    })
    .mouseenter(debounce(onCategoryMouseEnter, headerMenuDebounceDelay))
    .mouseleave(onTopLevelCategoryItemsMouseLeave);

  $headerContainer
    .mouseover(onHeaderContainerMouseOver);
};


module.exports = function () {
  let windowWidth = $(window).innerWidth();

  $(window).on('resize', function() {
    windowWidth = $(window).innerWidth();
  });

  if (windowWidth > CONFIG.breakpointMedium) {
    $(SELECTORS.window).on('load', initEvents);
  }
};

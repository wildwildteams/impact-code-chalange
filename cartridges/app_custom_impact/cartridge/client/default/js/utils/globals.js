export const CLASSES = {
    active: 'active',
    disableScroll: 'h-disable-scroll',
    showMenu: 'h-show-menu',
    isShow: 'is-show'
};

export var GlobalVars = {
    enableScroll: true,
    openedMobileMenu: false
};

export const CONFIG = {
    headerMenuDebounceDelay: 300,
    breakpointMedium: 1024
};

const { CLASSES, CONFIG, GlobalVars } = require('./globals');

let offset = 0;

/**
 * Enable/Disable scroll on page
 *
 * @param {boolean} state - true or false
 */
export function disableScroll(state) {
    const $body = $('body');

    if (state) {
        offset = $(window).scrollTop();

        $body.css('top', `${-offset}px`).addClass(CLASSES.disableScroll);
        GlobalVars.enableScroll = false;
    } else {
        $body.removeClass(CLASSES.disableScroll).css('top', 0);
        window.scrollTo(0, offset);

        setTimeout(function () {
            GlobalVars.enableScroll = true;
        }, CONFIG.transitionDelay);
    }
}

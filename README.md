# IMPACT code challenge
![Impact mega menu](https://i.ibb.co/sPR5GQR/2021-01-21-00-20-50.png)

### How menu looks?
You can see how the menu looks and works by watching the video on Google Disk.
[MegaMenu view on Google Disk](https://drive.google.com/drive/folders/1Pq2EDGibAHzXcb_toDAU3iYmbWBEiGUU?usp=sharing)

### How setup the project?
- Clone repo
- Install dependency using  `npm install`
- Create default `dw.json` file
- import content assets & slots from `data` folder, to instance
### npm commands
To create a cartridge, I used a standard set of sgmf-scripts, so there are standard commands.
-  `npm run uploadCartridge`
-  `npm run compile:js`
-  `npm run compile:scss`
-  `npm run watch`

### What was implemented in the test task?
- Removed recursion in the menuItem file (which could negatively affect performance)
- Created mobile and desktop versions of mega menus   (used a mobile first approach to improve performance)
- The menu contains slots with category context, as well as dynamic content assets for menu items.
